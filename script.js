fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));

// 5 & 6
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" has a status of "${json.completed}"`));

// 7
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: 'Created To Do List Item',
		userID: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// 8 & 9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		dataCompleted: "Pending",
		description: 'Toupdate the my to do list with a  different data structure',
		id: 1,
		status: "Pending",
		title: "Updated TO Do List Item",
		userId: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// 10 & 11
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: "07/09/21",
		id: 1,
		status: "Complete",
		title: "delectus aut autem",
		userId: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// 12
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})